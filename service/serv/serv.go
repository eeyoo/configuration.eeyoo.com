package serv

import (
	"log"

	"configuration.eeyoo.com/db"
)

type Service struct {
	Id     int    `json:"id"`
	Name   string `json:"name"`
	Status int    `json:"status"`
}

// QueryServiceList 查询服务列表
func QueryServiceList() (res []Service, err error) {
	data, err := db.QueryServiceList()
	if err != nil {
		log.Println("QueryServiceList err:", err)
		return
	}

	for _, v := range data {
		d := Service{Id: v.Id, Name: v.Name, Status: v.Status}
		res = append(res, d)
	}

	return
}
