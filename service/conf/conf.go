package conf

import (
	"log"

	"configuration.eeyoo.com/db"
)

type Config struct {
	Id    int    `json:"id"`
	Key   string `json:"key"`
	Value string `json:"value"`
}

// QueryUserByUserName 查询用户
func QuerySysConfigList(serviceId, envId int) (res []Config, err error) {
	data, err := db.QuerySysConfigList(serviceId, envId)
	if err != nil {
		log.Println("QuerySysConfigList err:", err)
		return
	}

	for _, v := range data {
		d := Config{Id: v.Id, Key: v.Key, Value: v.Value}
		res = append(res, d)
	}

	return
}
