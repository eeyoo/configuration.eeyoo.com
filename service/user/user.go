package user

import (
	"log"

	db "configuration.eeyoo.com/db"
)

type User struct {
	UserName      string `json:"username"`
	Password      string `json:"password"`
	UniqueID      string `json:"unique_id"`
	LastLoginTime string `json:"last_login_time"`
}

// QueryUserByUserName 查询用户
func QueryUserByUserName(userName, password string) (u User, err error) {
	user, err := db.QueryUserByUserName(userName, password)
	if err != nil {
		log.Println("QueryUserByUserName err:", err)
		return
	}
	return User{
		UserName:      user.UserName,
		Password:      user.Password,
		UniqueID:      user.UniqueID,
		LastLoginTime: user.LastLoginTime,
	}, nil
}
