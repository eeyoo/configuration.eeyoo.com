package db

import (
	"log"

	mydb "configuration.eeyoo.com/db/mysql"
)

type SysConfig struct {
	Id        int
	Key       string
	Value     string
	Status    int
	CreatedAt string
	UpdatedAt string
}

// QuerySysConfigList 数据库查询系统配置
func QuerySysConfigList(serviceId, envId int) (res []SysConfig, err error) {
	stmt, err := mydb.DBConn().Prepare("select id, conf_key, conf_value, conf_status, created_at, updated_at from t_sys_config where service_id=? and env_id=? order by updated_at desc")
	if err != nil {
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query(serviceId, envId)
	if err != nil {
		log.Printf("QuerySysConfigList stmt query err: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var d SysConfig
		err = rows.Scan(&d.Id, &d.Key, &d.Value, &d.Status, &d.CreatedAt, &d.UpdatedAt)
		if err != nil {
			log.Printf("t_sys_config query rows query err: %v", err)
			continue
		}
		res = append(res, d)
	}
	return
}
