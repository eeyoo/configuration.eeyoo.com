package db

import (
	"log"

	mydb "configuration.eeyoo.com/db/mysql"
)

type SysService struct {
	Id        int
	Name      string
	Status    int
	CreatedAt string
	UpdatedAt string
}

// QueryServiceList 数据库查询服务列表
func QueryServiceList() (res []SysService, err error) {
	stmt, err := mydb.DBConn().Prepare("select id, serv_name, serv_status, created_at, updated_at from t_sys_service")
	if err != nil {
		return
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		log.Printf("QueryServiceList stmt query err: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var d SysService
		err = rows.Scan(&d.Id, &d.Name, &d.Status, &d.CreatedAt, &d.UpdatedAt)
		if err != nil {
			log.Printf("t_sys_service query rows query err: %v", err)
			continue
		}
		res = append(res, d)
	}
	return
}
