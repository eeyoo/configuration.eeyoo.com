package mysql

import (
	"database/sql"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

func init() {
	db, _ = sql.Open("mysql", "root:root@tcp(127.0.0.1:3307)/config_center?charset=utf8")
	db.SetMaxIdleConns(1000)
	if err := db.Ping(); err != nil {
		log.Printf("connect mysql db err: %v", err)
		os.Exit(1)
	}
	log.Printf("init mysql connect success")
}

// DBConn mysql db connection
func DBConn() *sql.DB {
	return db
}
