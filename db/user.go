package db

import (
	mydb "configuration.eeyoo.com/db/mysql"
)

type SysUser struct {
	Id            int
	UserName      string
	Password      string
	UniqueID      string
	LastLoginTime string
	Status        int
	CreatedAt     string
	UpdatedAt     string
}

// QueryUserByUserName 数据库查询用户
func QueryUserByUserName(userName, password string) (res *SysUser, err error) {
	stmt, err := mydb.DBConn().Prepare("select id, user_name, user_password, user_uniq_id, last_login_time, user_status, created_at, updated_at from t_sys_user where user_name=? and user_password=?")
	if err != nil {
		return
	}
	defer stmt.Close()

	res = &SysUser{}
	err = stmt.QueryRow(userName, password).Scan(&res.Id, &res.UserName, &res.Password, &res.UniqueID, &res.LastLoginTime, &res.Status, &res.CreatedAt, &res.UpdatedAt)
	return
}
