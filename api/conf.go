package api

import (
	"log"
	"net/http"
	"strconv"

	"configuration.eeyoo.com/service/conf"
)

// ConfListHandler ...
func ConfListHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	r.ParseForm()
	serviceid, _ := strconv.Atoi(r.Form.Get("service_id"))
	envid, _ := strconv.Atoi(r.Form.Get("env_id"))
	log.Printf("service_id %d, env_id %d", serviceid, envid)
	resp := Resp{}
	data, err := conf.QuerySysConfigList(serviceid, envid)
	if err != nil {
		log.Println("ConfListHandler err: ", err)
		w.Write(resp.Failed(1, "获取系统配置失败"))
		return
	}

	w.Write(resp.Success(data))
	return
}
