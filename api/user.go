package api

import (
	"log"
	"net/http"

	"configuration.eeyoo.com/service/user"
)

// UserLoginHandler 用户登陆
func UserLoginHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	r.ParseForm()
	username := r.Form.Get("username")
	password := r.Form.Get("password")

	resp := Resp{}
	u, err := user.QueryUserByUserName(username, password)
	if err != nil {
		log.Println("UserLoginHandler err: ", err)
		w.Write(resp.Failed(1, "用户验证失败"))
		return
	}
	// 如果登陆成功，生成token，更新用户唯一ID和上次登陆时间，然后写缓存

	// 过滤器 拦截所有路由增加跨域功能
	// 过滤器 过滤/login路由进行用户验证

	w.Write(resp.Success(u))
	return
}
