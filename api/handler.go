package api

import (
	"encoding/json"
	"log"
	"net/http"
)

type Resp struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func (r *Resp) Json() []byte {
	b, _ := json.Marshal(r)
	return b
}

func (r *Resp) Success(data interface{}) []byte {
	r.Status = 0
	r.Message = "success"
	r.Data = data
	return r.Json()
}

func (r *Resp) Failed(status int, message string) []byte {
	r.Status = status
	r.Message = message
	return r.Json()
}

type Item struct {
	Id    int    `json:"id"`
	Key   string `json:"key"`
	Value string `json:"value"`
}

func New(id int, key, value string) Item {
	return Item{id, key, value}
}

func ApiHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	env := r.Form.Get("env")
	log.Println("env:", env)

	data := make([]Item, 0)
	if env == "online" {
		data = append(data, New(1, "mysql.host", "127.0.0.1"))
		data = append(data, New(2, "mysql.username", "root"))
		data = append(data, New(3, "mysql.password", "root"))
	} else if env == "gray_a" {
		data = append(data, New(1, "mysql.host", "127.0.0.2"))
		data = append(data, New(2, "mysql.username", "good"))
		data = append(data, New(3, "mysql.password", "good"))
	} else {
		data = append(data, New(1, "mysql.host", "127.0.0.11"))
		data = append(data, New(2, "mysql.username", env))
		data = append(data, New(3, "mysql.password", env))
	}

	res := Resp{0, "成功", data}
	b, _ := json.Marshal(res)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	//w.Header().Set("Origin", "http://localhost:8081")
	w.Write(b)
}
