package api

import (
	"log"
	"net/http"

	"configuration.eeyoo.com/service/serv"
)

// ServListHandler ...
func ServListHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	resp := Resp{}
	data, err := serv.QueryServiceList()
	if err != nil {
		log.Println("ConfListHandler err: ", err)
		w.Write(resp.Failed(1, "获取服务列表失败"))
		return
	}

	w.Write(resp.Success(data))
	return
}
