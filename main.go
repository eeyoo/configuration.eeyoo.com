package main

import (
	"fmt"
	"log"
	"net/http"

	"configuration.eeyoo.com/api"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}

func main() {
	http.HandleFunc("/hello", handler)
	http.HandleFunc("/api", api.ApiHandler)
	http.HandleFunc("/user/login", api.UserLoginHandler)
	http.HandleFunc("/conf/list", api.ConfListHandler)
	http.HandleFunc("/serv/list", api.ServListHandler)
	log.Fatal(http.ListenAndServe(":8081", nil))
}
