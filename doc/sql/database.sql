CREATE TABLE `t_sys_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serv_name` varchar(20) NOT NULL DEFAULT '' COMMENT '系统服务',
  `serv_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '标志（0可用/1禁用/2已删除等状态）',
  `created_at` datetime DEFAULT NOW() COMMENT '创建时间',
  `updated_at` datetime DEFAULT NOW() on update CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统服务字典';

CREATE TABLE `t_sys_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conf_key` varchar(20) NOT NULL DEFAULT '' COMMENT '系统配置key',
  `conf_value` varchar(20) NOT NULL DEFAULT '' COMMENT '系统配置value',
  `service_id` int NOT NULL DEFAULT '0' COMMENT '系统服务ID',
  `env_id` int NOT NULL DEFAULT '0' COMMENT '环境ID',
  `conf_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '标志（0可用/1禁用/2已删除等状态）',
  `created_at` datetime DEFAULT NOW() COMMENT '创建时间',
  `updated_at` datetime DEFAULT NOW() on update CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统配置信息';

CREATE TABLE `t_sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名称',
  `user_password` varchar(10) NOT NULL DEFAULT '' COMMENT '用户密码',
  `user_uniq_id` varchar(50) NOT NULL DEFAULT '' COMMENT '用户唯一ID',
  `last_login_date` date NOT NULL COMMENT '上次登陆日期',
  `last_login_time` datetime NOT NULL COMMENT '上次登陆时间',
  `user_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '标志（0可用/1禁用/2已删除等状态）',
  `created_at` datetime DEFAULT NOW() COMMENT '创建时间',
  `updated_at` datetime DEFAULT NOW() on update CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY(`id`),
  INDEX `idx_user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户信息';